<?php if (have_posts()): while (have_posts()) : the_post(); ?>
<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );?>

    <!-- article -->
    <article id="post-<?php the_ID(); ?>" <?php post_class('uk-margin-large-bottom'); ?>>

        <div class="uk-margin-large-bottom" property="image">
            <!-- post thumbnail -->
            <?php if (has_post_thumbnail()) : // Check if thumbnail exists ?>
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <div class="thumbnail-category" style="background-image: url('<?php echo $src[0]; ?>');"></div>
                </a>
            <?php endif; ?>
            <!-- /post thumbnail -->
        </div>

        <div class="uk-margin-bottom">
            <ul class="uk-subnav uk-subnav-divider">
                <li>
                    <!-- date -->
                    <span class="date">
                        <?php the_time('j F Y'); ?>
                    </span>
                </li>
                <li>
                    <span class="author">
                        <?php _e('Published by', 'html5blank'); ?> <?php the_author_posts_link(); ?>
                    </span>
                </li>
            </ul>

            <h2 class="uk-article-title uk-margin-remove-top">
                <a class="uk-link-reset" href="<?php the_permalink(); ?>"
                   title="<?php the_title(); ?>"><?php the_title(); ?></a>
            </h2>
        </div>

        <div class="article-text" property="text">
            <?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>
        </div>

        <!--		--><?php //edit_post_link(); ?>

    </article>
    <!-- /article -->

    <hr class="uk-margin-large">

<?php endwhile; ?>

<?php else: ?>

    <!-- article -->
    <article>
        <h2><?php _e('Sorry, nothing to display.', 'html5blank'); ?></h2>
    </article>
    <!-- /article -->

<?php endif; ?>
