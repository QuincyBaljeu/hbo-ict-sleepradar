<?php /* Template Name: Beta template */

get_header(); ?>

    <main role="main">

        <section class="uk-section uk-section-large">
            <div class="uk-container uk-container-small">

                <div class="uk-grid-large" uk-grid>
                        <div class="uk-panel">

                            <h1><?php the_title(); ?></h1>

                            <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                                <!-- article -->
                                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                                    <?php the_content(); ?>

                                    <?php comments_template('', true); // Remove if you don't want comments ?>

                                    <br class="clear">

                                    <?php edit_post_link(); ?>

                                </article>
                                <!-- /article -->

                            <?php endwhile; ?>

                            <?php else: ?>

                                <!-- article -->
                                <article>

                                    <h2><?php _e('Sorry, nothing to display.', 'html5blank'); ?></h2>

                                </article>
                                <!-- /article -->

                            <?php endif; ?>

                        </div>
                </div>

            </div>
        </section>
    </main>

<?php get_footer(); ?>