<?php get_header(); ?>

<main role="main">

    <section class="uk-section uk-section-large">
        <div class="uk-container">

            <div class="uk-grid-large" uk-grid>
                <div class="uk-width-2-3@m">

                    <div class="uk-panel">
                        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                            <!-- article -->
                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                                <div class="uk-margin-large-bottom" property="image">
                                    <!-- post thumbnail -->
                                    <?php if (has_post_thumbnail()) : // Check if thumbnail exists ?>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                            <?php the_post_thumbnail('full'); // Declare pixel size you need inside the array ?>
                                        </a>
                                    <?php endif; ?>
                                    <!-- /post thumbnail -->
                                </div>

                                <div class="uk-margin-bottom">

                                    <ul class="uk-subnav uk-subnav-divider">
                                        <li>
                                            <span class="date">
                                                <?php the_time('j F Y'); ?>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="author">
                                                <?php _e('Published by', 'html5blank'); ?> <?php the_author_posts_link(); ?>
                                            </span>
                                        </li>
                                    </ul>

                                    <h1 class="uk-article-title uk-margin-remove-top">
                                        <a class="uk-link-reset" href="<?php the_permalink(); ?>"
                                           title="<?php the_title(); ?>"><?php the_title(); ?></a>
                                    </h1>
                                </div>

                                <?php the_content(); // Dynamic Content ?>

                                <!-- comments -->
                                <?php comments_template(); ?>

                                <?php //edit_post_link(); // Always handy to have Edit Post Links available ?>

                            </article>
                            <!-- /article -->

                        <?php endwhile; ?>

                        <?php else: ?>

                            <!-- article -->
                            <article>

                                <h1><?php _e('Sorry, nothing to display.', 'html5blank'); ?></h1>

                            </article>
                            <!-- /article -->

                        <?php endif; ?>
                    </div>

                </div>

                <div class="uk-width-1-3@m">
                    <div class="uk-panel">
                        <?php get_sidebar(); ?>
                    </div>
                </div>

            </div>

        </div>
    </section>
</main>

<?php get_footer(); ?>
