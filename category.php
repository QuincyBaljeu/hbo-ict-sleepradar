<?php get_header(); ?>

<main role="main">

    <section class="sub-page uk-section uk-section-large">
        <div class="uk-container">

            <div class="uk-grid-large" uk-grid>

                <div class="uk-width-2-3@m">
                    <div class="uk-panel">

                        <div uk-grid>
                            <div class="uk-width-1-1@m">
                                <h1><?php single_cat_title(); ?></h1>
                            </div>
                        </div>

                        <div uk-grid>
                            <div class="uk-width-1-1@m">
                                <?php get_template_part('loop'); ?>
                                <?php get_template_part('pagination'); ?>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="uk-width-1-3@m">
                    <div class="uk-panel">
                        <?php get_sidebar(); ?>
                    </div>
                </div>

            </div>

        </div>
    </section>

</main>

<?php get_footer(); ?>
