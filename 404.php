<?php get_header(); ?>

    <main role="main">

        <section class="uk-section uk-section-large">
            <div class="uk-container">

                <div uk-grid>
                    <div class="uk-width-2-3@m">

                        <!-- article -->
                        <article id="post-404">

                            <h1>Pagina niet gevonden</h1>
                            <p>
                                Helaas kon de door u opgevraagde pagina niet worden gevonden. Het kan zijn dat de pagina is
                                verwijderd of dat het adres in de browser niet correct is gespeld. Controleer de spelling en probeer
                                het opnieuw.
                            </p>

                            <hr class="uk-margin-medium">

                            <a href="<?php echo home_url(); ?>">Terug naar de homepage?</a>


                        </article>
                        <!-- /article -->

                        <!-- Deze moet aangepast worden -->

                    </div>
                </div>

            </div>
        </section>
    </main>

<?php get_footer('sub'); ?>