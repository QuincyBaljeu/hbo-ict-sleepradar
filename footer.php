			<!-- footer -->
			<footer class="footer uk-section uk-section-large">

                <div class="uk-container">
                    <div class="uk-child-width-1-5@m uk-grid-medium" uk-grid>

                        <div>
                            <div class="uk-panel">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/s.png" alt="Logo-small" class="logo-img-small">
                            </div>
                        </div>

                        <div>
                            <div class="uk-panel">
                                <?php dynamic_sidebar('footer_widget_1'); ?>
                            </div>
                        </div>

                        <div>
                            <div class="uk-panel">
                                <?php dynamic_sidebar('footer_widget_2'); ?>
                            </div>
                        </div>

                        <div>
                            <div class="uk-panel contact-info">
                                <?php dynamic_sidebar('footer_widget_3'); ?>          
                                <div class="social-icons">
                                    <a href="https://www.facebook.com/sleepradar" target="_blank">
                                         <i class="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <div class="social-icons">
                                    <a href="https://twitter.com/sleepradar" target="_blank">
                                         <i class="fa fa-twitter" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="uk-panel">
                                <?php dynamic_sidebar('footer_widget_4'); ?>
                            </div>
                        </div>

                    </div>
                </div>

			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
			(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
			(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
			l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
			ga('send', 'pageview');
		</script>

	</body>
</html>