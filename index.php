<?php get_header(); ?>

<main role="main">

    <!-- section 1 -->
    <section class="section-1 uk-section uk-section-large">

        <div class="uk-container ">
            <div class="uk-child-width-1-2@m uk-grid-large" uk-grid>

                <!-- Wij maken je bed slimmer -->
                <div class="uk-flex uk-flex-middle">
                    <?php $query = new WP_Query(
                        array(
                            'page_id' => 20, //pagina id
                        )
                    ); ?>

                    <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                        <!-- Get URL to make it a background image easy for responsive -->
                        <?php $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array(5600, 1000), false, ''); ?>

                        <div class="post-holder">

                            <div class="post">
                                <h5><?php the_secondary_title(); ?></h5>
                                <!-- Display the Title as a link to the Post's permalink. -->
                                <h4>
                                    <a href="<?php the_permalink() ?>" rel="bookmark"
                                       title="Permanent Link to <?php the_title_attribute(); ?>">
                                        <?php the_title(); ?>
                                    </a>
                                </h4>

                                <div class="entry">
                                    <?php echo apply_filters('the_excerpt', get_the_excerpt()); ?>
                                </div>

                            </div> <!-- closes the first div box -->
                        </div>

                    <?php endwhile;
                        wp_reset_postdata();
                    else : ?>
                        <p><?php esc_html_e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php endif; ?>
                </div>

                <!-- Video uitleg Sleepradar -->
                <div>
                    <div class="image-holder">
                        <?php dynamic_sidebar('video_widget'); ?>
                    </div>
                </div>

            </div>
        </div>

    </section>
    <!-- /section 1 -->

    <!-- section 2 -->
    <section class="section-2 uk-section uk-section-large">

        <div class="uk-container ">
            <div class="uk-child-width-1-2@m uk-grid-large" uk-grid>

                <div>
                    <?php $query = new WP_Query(
                        array(
                            'page_id' => 5, //pagina id
                        )
                    ); ?>

                    <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                        <!-- Get URL to make it a background image easy for responsive -->
                        <?php $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array(5600, 1000), false, ''); ?>
                        <div class="uk-flex uk-flex-center">
                            <?php the_post_thumbnail(); ?>
                        </div>
                    <?php endwhile;
                        wp_reset_postdata();
                    else : ?>
                        <p><?php esc_html_e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php endif; ?>
                </div>

                <div class="uk-flex uk-flex-middle">
                    <?php $query = new WP_Query(
                        array(
                            'page_id' => 5, //pagina id
                        )
                    ); ?>

                    <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

                        <div class="post-holder">
                            <div class="post">
                                <h5><?php the_secondary_title(); ?></h5>
                                <!-- Display the Title as a link to the Post's permalink. -->
                                <h4>
                                    <a href="<?php the_permalink() ?>" rel="bookmark"
                                       title="Permanent Link to <?php the_title_attribute(); ?>">
                                        <?php the_title(); ?>
                                    </a>
                                </h4>

                                <div class="entry app-entry">
                                    <?php echo apply_filters('the_excerpt', get_the_excerpt()); ?>
                                    <div class="excerpt download">
                                        <a href="https://itunes.apple.com/nl/app/sleepradar/id1084146490?mt=8" target="_blank" class="view-article">
                                            iOS
                                        </a>
                                    </div>
                                    <a href="https://play.google.com/store/apps/details?id=com.sleepradar.app" target="_blank" class="view-article">
                                        Android
                                    </a>
                                </div>

                            </div>
                        </div>

                    <?php endwhile;
                        wp_reset_postdata();
                    else : ?>
                        <p><?php esc_html_e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php endif; ?>
                </div>


            </div>
        </div>

    </section>
    <!-- /section 2 -->

    <!-- section 3 -->
    <section class="section-3 uk-section uk-section-large">

        <div class="uk-container ">
            <div class="uk-child-width-1-2@m uk-grid-large" uk-grid>

                <div class="uk-flex uk-flex-middle">
                    <?php $query = new WP_Query(
                        array(
                            'page_id' => 24, //pagina id
                        )
                    ); ?>

                    <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

                        <div class="post-holder">
                            <div class="post">
                                <h5><?php the_secondary_title(); ?></h5>
                                <!-- Display the Title as a link to the Post's permalink. -->
                                <h4>
                                    <a href="<?php the_permalink() ?>" rel="bookmark"
                                       title="Permanent Link to <?php the_title_attribute(); ?>">
                                        <?php the_title(); ?>
                                    </a>
                                </h4>

                                <div class="entry">
                                    <?php echo apply_filters('the_excerpt', get_the_excerpt()); ?>
                                </div>
                            </div> <!-- closes the first div box -->
                        </div>

                    <?php endwhile;
                        wp_reset_postdata();
                    else : ?>
                        <p><?php esc_html_e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php endif; ?>
                </div>

                <div>

                    <?php $query = new WP_Query(
                        array(
                            'page_id' => 24, //pagina id
                        )
                    ); ?>

                    <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

                        <div class="uk-flex uk-flex-center">
                            <?php the_post_thumbnail(); ?>
                        </div>

                    <?php endwhile;
                        wp_reset_postdata();
                    else : ?>
                        <p><?php esc_html_e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php endif; ?>
                </div>

            </div>
        </div>

    </section>
    <!-- /section 3 -->

    <!-- section 4 -->
    <section class="section-4 uk-section uk-section-large">
        <div class="uk-container ">

            <div uk-grid>
                <div class="uk-width-1-1@m">
                    <div class="uk-panel">
                        <h1>
                            Blog
                        </h1>
                        <h2>
                            Blijf op de hoogte
                        </h2>
                    </div>
                </div>
            </div>

            <div class="uk-grid-large" uk-grid uk-height-match="target: .uk-panel">

                <?php $query = new WP_Query(
                    array(
                        'cat' => 3, //categorie id
                        'order' => 'DESC', //of DESC
                        'orderby' => 'date', //of title, date, none, etc
                        'posts_per_page' => 2 //blog posts per page
                    )
                ); ?>

                <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                    <?php $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array(5600, 1000), false, ''); ?>

                    <div class="uk-width-1-2@m">

                            <div class="uk-grid-collapse" uk-grid uk-height-match="target: .uk-panel">
                                <div class="uk-width-1-2@m">
                                    <div class="uk-panel inner-image"
                                         style="background-image: url('<?php echo $src[0]; ?>');"></div>
                                </div>

                                <div class="uk-width-1-2@m">
                                    <div class="uk-panel uk-position-relative blog-post uk-padding">

                                        <!-- Display the date (November 16th, 2009 format) and a link to other posts by this posts author. -->
                                        <small><?php the_time('j F Y'); ?></small>
                                        <!-- Display the Title as a link to the Post's permalink. -->
                                        <h3>
                                            <a href="<?php the_permalink() ?>" rel="bookmark"
                                               title="Permanent Link to <?php the_title_attribute(); ?>">
                                                <?php the_title(); ?>
                                            </a>
                                        </h3>
                                        <div class="entry">
                                            <?php echo apply_filters('the_excerpt', get_the_excerpt()); ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                    </div>

                <?php endwhile;
                    wp_reset_postdata();
                else : ?>
                    <p><?php esc_html_e('Sorry, no posts matched your criteria.'); ?></p>
                <?php endif; ?>

            </div>

            <div uk-grid>
                <div class="uk-width-1-1@m uk-flex uk-flex-right@s uk-flex-center">
                    <div class="uk-panel">
                        <a href="<?php echo get_category_link(3); ?>" class="button button-all view-article">Bekijk alle berichten</a>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- /section 4 -->

    <!-- section 5 -->
    <section class="section-5 uk-section uk-section-large uk-visible@m">
        <div class="uk-container ">

            <div uk-grid>
                <div class="uk-width-1-1@m">
                    <div class="uk-panel">
                        <h1>
                            Social
                        </h1>
                        <h2>
                            Volg ons avontuur
                        </h2>
                        <?php echo do_shortcode('[custom-facebook-feed]'); ?>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- /section 5 -->

    <!-- section 6 -->
    <section class="section-6 uk-section uk-section-large">
        <div class="uk-container ">

            <div uk-grid>
                <div class="uk-width-1-1@m">
                    <div class="uk-panel">
                        <h1>
                            Support
                        </h1>
                        <h2>
                            Gefinancierd door de volgende partijen
                        </h2>
                    </div>
                </div>
            </div>

            <div uk-grid>
                <div class="uk-width-1-1@m">
                    <div class="uk-panel">
                        <?php dynamic_sidebar('logo_slider_widget'); ?>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- /section 5 -->

</main>

<?php get_footer(); ?>
