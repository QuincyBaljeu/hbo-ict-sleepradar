<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">

    <title>
        <?php wp_title(''); ?>

        <?php if (wp_title('', false)) {
            echo ' | ';
        } ?>

        <?php bloginfo('name'); ?><?php bloginfo('description'); ?>
    </title>

    <link href="//www.google-analytics.com" rel="dns-prefetch">

    <!-- view point -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <!-- /view point -->

    <!-- CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.30/css/uikit.min.css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css">
    <!-- /CSS -->

    <!-- Javascript -->
    <!-- jQuery is required -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- UIkit JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.30/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.30/js/uikit-icons.min.js"></script>

    <!-- Custom JS -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/myScript.js"></script>

    <!-- /Javascript -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/fontawesome-markers.min.js"></script>

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180"
          href="<?php echo get_template_directory_uri(); ?>/img/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32"
          href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16"
          href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/img/icons/manifest.json">
    <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/img/icons/safari-pinned-tab.svg"
          color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <!-- /Favicon -->

    <!-- Kleur-voor-browsers -->
    <!-- Chrome, Firefox OS, Opera and Vivaldi -->
    <meta name="theme-color" content="#67a2b9">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#67a2b9">
    <!-- iOS Safari *LET OP! Kan alleen zwart, wit of transparant. ( ‘black’, ‘black-translucent’ of ‘default’ ) -->
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- /Kleur-voor-browsers -->

    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<!-- This is the off-canvas -->
<div id="tm-mobile" uk-offcanvas="overlay: true">
    <div class="uk-offcanvas-bar uk-flex uk-flex-column">

        <ul class="uk-nav uk-nav-primary uk-nav-center">

            <li class="uk-nav-header">
                <a href="<?php echo home_url(); ?>" class="uk-navbar-item uk-logo">
                    <!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
                    <img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="logo">
                </a>
            </li>

            <li class="uk-parent">
                <?php html5blank_nav(); ?>
                <div class="social-icons"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                </div>
                <div class="social-icons"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></div>
            </li>

        </ul>

    </div>
</div>

<!-- wrapper -->
<div class="wrapper">

    <!-- header -->
    <header class="header clear" role="banner">

        <!-- mobile menu -->
        <div id="main-bar-mobile" class="uk-container uk-container-expand uk-hidden@m">
            <nav class="uk-navbar-container uk-navbar" uk-navbar="">

                <div class="uk-navbar-left">
                    <a href="<?php echo home_url(); ?>" class="uk-navbar-item uk-logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="logo">
                    </a>
                </div>

                <div class="uk-navbar-right">

                    <ul class="uk-navbar-nav">
                        <?php pll_the_languages(array('show_flags'=>1,'show_names'=>0)); ?>
                        <li>
                            <a href="https://www.facebook.com/sleepradar" target="_blank"><span class="facebook-icon"></span></a>
                        </li>
                        <li>
                            <a href="https://twitter.com/sleepradar" target="_blank"><span class="twitter-icon"></span></a>
                        </li>
                        <li>
                            <a class="uk-navbar-toggle" href="#tm-mobile" uk-toggle="">
                                <div uk-navbar-toggle-icon="" class="uk-navbar-toggle-icon uk-icon"></div>
                                MENU
                            </a>
                        </li>
                    </ul>

                </div>

            </nav>
        </div>

        <!-- main menu -->
        <div id="main-bar" class="uk-visible@m" uk-header>
            <nav class="uk-navbar-container">

                <div class="uk-container">
                    <nav class="uk-navbar" uk-navbar={}>

                        <div class="uk-navbar-left">
                            <a href="<?php echo home_url(); ?>" class="uk-navbar-item uk-logo">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="logo">
                            </a>
                        </div>

                        <div class="uk-navbar-right">

                            <?php html5blank_nav(); ?>

                            <ul class="uk-navbar-nav"><!-- 
                                <li>
                                    <a href="https://www.facebook.com/sleepradar" target="_blank"><span class="facebook-icon"></span></a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/sleepradar" target="_blank"><span class="twitter-icon"></span></a>
                                </li> -->
                                <?php pll_the_languages(array('show_flags'=>1,'show_names'=>0)); ?>
                            </ul>

                        </div>
                    </nav>
                </div>

            </nav>
        </div>

        <!-- slider -->
        <div class="slider">
            <?php if (is_page('contact')) { ?>
                <?php get_template_part('google-map'); ?>
            <?php } else {
                dynamic_sidebar('slider_widget');
            } ?>
        </div>
        <!-- /slider -->

    </header>
    <!-- /header -->
