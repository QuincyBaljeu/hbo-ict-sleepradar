<div id="map_wrapper"><div id="map_canvas" class="mapping"></div></div>

<script type="text/javascript">
    jQuery(function ($) {
        // Asynchronously Load the map API
        var script = document.createElement('script');
        script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyDptpTC9IExdWpAC5slid2yoL_I0hAyzjQ&callback=initialize";
        document.body.appendChild(script);
    });

    var color = "#444"; //Set your tint color. Needs to be a hex value.

    function initialize() {
        var map;
        var bounds = new google.maps.LatLngBounds();

        var styles = [
            {"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}
        ];

        var mapOptions = {
            mapTypeControlOptions: {
                mapTypeIds: ['Styled']
            },

            scrollwheel: true,
            navigationControl: true,
            mapTypeControl: false,
            zoomControl: true,
            disableDefaultUI: true,
            mapTypeId: 'Styled',
            gestureHandling: 'cooperative'
        };

        // Display a map on the page
        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
        map.setTilt(45);

        var styledMapType = new google.maps.StyledMapType(styles, {name: 'Styled'});
        map.mapTypes.set('Styled', styledMapType);

        // Multiple Markers
        var markers = [
            ['Nieuwvlietseweg 39, 4504 AD Nieuwvliet', 51.3726777, 3.4743581]
        ];

        // Display multiple markers on a map
        var infoWindow = new google.maps.InfoWindow(), marker, i;

        // Loop through our array of markers & place each one on the map
        for (i = 0; i < markers.length; i++) {
            var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
            bounds.extend(position);
            marker = new google.maps.Marker({
                animation: google.maps.Animation.DROP,
                map: map,
                icon: {
                    path: fontawesome.markers.MAP_MARKER,
                    scale: 0.8,
                    strokeWeight: 0,
                    strokeColor: '#003e4d',
                    strokeOpacity: 0,
                    fillColor: '#003e4d',
                    fillOpacity: 1
                },
                position: position,
                title: markers[i][0]
            });

            // Allow each marker to have an info window
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infoWindow.setContent(infoWindowContent[i][0]);
                    infoWindow.open(map, marker);
                }
            })(marker, i));

            // Automatically center the map fitting all markers on the screen
            map.fitBounds(bounds);
        }

        // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
        var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
            this.setZoom(13);
            google.maps.event.removeListener(boundsListener);
        });

    }
</script>