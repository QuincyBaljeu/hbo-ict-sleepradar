<?php /* Template Name: Contact Template */

get_header(); ?>

    <main role="main">

        <section class="uk-section uk-section-large">
            <div class="uk-container">

                <div class="uk-grid-large" uk-grid>
                    <div class="uk-width-1-1@m">
                        <div class="uk-panel">
                            <h1><?php the_title(); ?></h1>
                        </div>
                    </div>
                </div>

                <div class="uk-grid-large" uk-grid>

                    <div class="uk-width-auto@m">
                        <div class="uk-panel">

                            <h3>E-mail</h3>
                            <ul class="uk-list">
                                <li><a href="mailto:paul@sleepradar.com" target="_blank">paul@sleepradar.com</a></li>
                            </ul>

                            <h3>Telefoon</h3>
                            <ul class="uk-list">
                                <li><a href="tel:+31 (0)6 12345678">+31 (0)6 12345678</a></li>
                            </ul>

                            <h3>Volg ons</h3>
                            <a class="uk-margin-small-right uk-icon-button uk-icon" uk-icon="icon: facebook"
                               href="https://www.facebook.com/sleepradar" target="_blank"></a>
                            <a class="uk-margin-small-right uk-icon-button uk-icon" uk-icon="icon: twitter"
                               href="https://twitter.com/sleepradar" target="_blank"></a>

                            <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                                <!-- article -->
                                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                                    <?php the_content(); ?>

                                    <?php comments_template('', true); // Remove if you don't want comments ?>

                                    <br class="clear">

                                    <?php edit_post_link(); ?>

                                </article>
                                <!-- /article -->

                            <?php endwhile; ?>

                            <?php else: ?>

                                <!-- article -->
                                <article>

                                    <h2><?php _e('Sorry, nothing to display.', 'html5blank'); ?></h2>

                                </article>
                                <!-- /article -->

                            <?php endif; ?>

                        </div>
                    </div>

                    <div class="uk-width-expand@m">
                        <div class="uk-panel">

                            <h3>Veel gestelde vragen</h3>

                            <ul uk-accordion>

                                <?php $query = new WP_Query(array(
                                    'post_type' => 'faq',
                                )) ?>
                                <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

                                    <li>
                                        <h3 class="uk-accordion-title"><?php the_title(); ?></h3>
                                        <div class="uk-accordion-content">
                                            <p><?php the_content(); ?></p>
                                        </div>
                                    </li>

                                <?php endwhile;
                                    wp_reset_postdata();
                                else : ?>
                                    <p><?php esc_html_e('Sorry, no posts matched your criteria.'); ?></p>
                                <?php endif; ?>

                            </ul>

                        </div>
                    </div>

                    <div class="uk-width-2-5@m">
                        <div class="uk-panel contact-page-sidebar">

                            <div class="uk-card uk-card-default contact-page-sidebar uk-card-body">
                                <?php
                                // Fetching current language
                                $currentlang = pll_current_language();
                                if ($currentlang == 'nl') {
                                    echo '<h3>Heeft u een vraag?</h3>';
                                    echo do_shortcode('[contact-form-7 id="12" title="Contactformulier"]');
                                }
                                if ($currentlang == 'en') {
                                    echo '<h3>Do you have a question?</h3>';
                                    echo do_shortcode('[contact-form-7 id="146" title="Contactformulier_copy"]');
                                }
                                ?>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </section>
    </main>

<?php get_footer(); ?>